## FABRICACIÓN DE VENENOS

### Venenos Sencillos

- **Kit de envenenador**.   
    - Todo lo necesario para cosechar y procesar cuidadosamente venenos naturales y venenos se puede encontrar en este kit.
    - Puedes intentar cosechar venenos de áreas con flora abundante, pasando una hora de búsqueda y haciendo un chequeo de kit de envenenador. 
        - Con un resultado de 1-14 te envenenas accidentalmente y sufres los efectos de un veneno básico. 
        - Con un resultado de 15-19 obtienes un vial de veneno básico. 
        - Con un resultado de 20-24 obtienes un vial de veneno avanzado, 
        - y con un resultado de 25 o más obtienes un vial de veneno potente.
    - Como alternativa, con 10 minutos de trabajo puedes puedes usar un kit de envenenador para cosechar veneno del cadáver fresco de una criatura que contenga veneno de forma natural. 
        - Haz un chequeo de kit de envenenador contra la DC del veneno de la criatura (si el veneno de la criatura no tiene, usa 8 + la CR de la criatura). 
        - Si fallas, sufres los efectos del veneno, y si tienes éxito cosechas un frasco.


### Uso de Alquimia
- bla bla… no da tiempo a explicaciones, el resumen es que en downtime se puede invertir en fabricar venenos. 
- Los comunes de la Lista de Equipo salen automáticamente, si se dispone de los medios adecuados: Instalaciones, equipación y componentes. 
- A esto se le añade la preparación del fabricante y si cuenta con alguna ayuda o ventaja adicional (componentes especiales, un mentor o siervos que ayuden, magia, etc).

- Si los venenos que se busca fabricar son exóticos, esto requiere obtener 
    - A – los conocimientos, ya sea por la adquisición de recetas o la ayuda de un pnj. 
    - B – Los componentes, que algunas veces tienen sustitutivos y otras veces han de ser muy concretos. 
    - C – Hay venenos que sólo se pueden hacer bajo determinadas condiciones. 

- Considerando que se ha superado A, B y C, hay que calcular los costes. 
- Se hacen 2 chequeos por decana para ver cómo va el proceso de fabricación, después de hacer el chequeo de Problemas propio del downtime. 
    - Es un cálculo de lo eficiente que es el fabricante, respecto a la mitad del coste básico del veneno. 
    - Se suma el bono de éstos factores y se multiplica por 5%. Ese es el ahorro que se consigue, en relación con la mitad del coste.

- 1d10 + competencia + característica + instalaciones (-2..3) + componentes (-2..5) + pericias [+ especial]

- Por ejemplo, si un buen alquimista quiere hacer un veneno de coste base 1000 mo y saca en el chequeo un 28, eso es 140% sobre 500, osea, consigue que su eficiencia descuente 700 mo del coste final, en ese lote (media decana).  



### Uso de Habilidades Especiales
- Los ladrones tienen rasgos y mecánicas (como Analysis) compatibles con el estudio de venenos, que les favorecen en estos chequeos. 



