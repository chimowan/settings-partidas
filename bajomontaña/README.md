# Setting Bajomontaña

Información relativa al Setting de la partida de Bajomontaña.
Ya sea la creación de personaje, modificaciones a las reglas y objetos, etc.


## Creación de Personaje
- Manual de Level Up
- Personajes de nivel 8
- **No** hay dote extra a nivel 1.
- Selección de dotes y conjuros limitados al manual de Level Up.
    - Conjuros de otros manuales se consideran infrecuentes y se consiguen en aventura o downtime.
- Se permite multiclase.
- Puntos de vida:
    - Máximo a nivel 1
    - La media o tirada en los siguientes niveles
- Características:
    - [Por puntos custom](https://chicken-dinner.com/5e/5e-point-buy.html). Se puede comprar hasta un 18 por 19 puntos.
    - Por tiradas: 4d6, guardando los 3 mejores.
    - Características adicionales:
        - Suerte: 4d6, quitar el peor
        - Apariencia: 4d6, quitar el peor + MOD Carisma
- Los personajes conocen tres maniobras del Battlemaster del manual del jugador de 5ª, y pueden activarlas gastando un punto de acción.
- Los humanos ganan un 10% de experiencia extra


### Equipo inicial
Se usa el Sane Magical Prices de referencia:
- 1500 monedas de oro en objetos de combate
- 2500 monedas de oro en objetos que no sean de combate
- 600 monedas de oro en consumibles (pociones a mitad de precio)
- Los magos pueden usar este dinero para tener más conjuros inscritos en su libro, o duplicar su libro de conjuros. El coste es únicamente el de inscribir el conjuro en el libro, no hace falta comprar el pergamino. Representa tiempo de estudio.
    - Pueden ser de su lista de conjuros, pero no los raros. Únicamente versiones comunes.
- Coste de armadura de adamantita: (precio * 1.5) + 500

## Conjuros y magias

### Conocidos/Preparados
- Los cantrips del manual de VaraNegra (The Blackstaff book of 1000 spells) se consideran como si fueran del manual básico y son de libre disposición dentro de la clase.
- Se conocen los que marque la tabla de la clase correspondiente.
Adicionalmente, se añade el bonus de competencia a los conjuros conocidos (bardo, hechicero,..) o preparados (mago, clérigo,...)
- También se añade el bonus de competencia a los cantrips conocidos
- Ejemplo: Competencia +3:
    - 3 cantrips extra conocidos
    - 3 conjuros extra conocidos/preparados.
- Este bonus **no** afecta a los que se ganen por dotes, rasgos, ...

### Cambios
- Druidas y clérigos pueden cambiar a la carta sus conjuros conocidos cada noche, de entre todos los del manual.
- Un mago tiene los cantrips que escoja tener memorizados y luego puede jugar con qué prepara de lo que tenga a mano, o sea, su libro/s de conjuros.
- **Mage Hand / Mano de Mago**: La mano tiene un bonus Fue = comp. del lanzador.
    - La DC Maniobra = 8 + comp*2. Se puede utilizar para realizar maniobras de combate básicas. 
    - Este uso violento obliga a realizar chequeos de habilidad mágica a DC 10.

### Componentes
- Los componentes se pueden sustituir por el foco, excepto cuando son un elemento clave en el desempeño del conjuro (que lo suele poner la descripción, y son pocos casos) o cuando es algo caro o exótico (que suele venir indicado con su coste en mo).
- Y hay componentes exóticos adicionales que pueden modificar y potenciar algunos conjuros

### Duración de los conjuros:
- La duración de los conjuros que duran más de un asalto, se multiplica por bonus comp.
- Los conjuros que duran justo un asalto y tienen un tiempo de lanzamiento de 1 acción, se pueden prolongar (hasta el bonus comp.) pasando a ser de concentración.

### Concentración
- Se puede uno concentrar en más de 1 conjuro, pero eso complica su DC de Concentración, aumentando en la suma de los niveles de los conjuros en los que se concentra. 
    - O sea, si tengo uno de 1º y ahora añado otro de 2º tengo un +3 a la DC de Concentración. 
- Y otra homerule es que la duración de los conjuros de más de 1 asalto se multiplica por tu bono de competencia. 
    - Así que uno de 1 minuto para un nivel 8 sería de 3 minutos
- Si falla la tirada de concentración, se pierden todos los conjuros.

### Conjuros de área
- Las criaturas que estén en los bordes del área tiran Suerte (para pnjs suele ser 10+ en 1d20) para ver si son afectadas.
    - La DC de SUERTE es 10 + nivel de conjuro
    -  Los casos obvios no lo requieren.
- El cantrip **Calculate** permite al lanzador encontrar en punto y el tiempo exacto para alcanzar a todos los objetivos, y fallan automáticamente la tirada de Sue.


### Contraconjuros y disipar
- Al disipar un conjuro, me interesa que se considere también el poder de cada caster, osea, que se cuenten el bonus de competencia de ambos lanzadores para las tiradas. 
- Osea, si lanzo un Counter contra un conj. n4 y mi característica es de +3 (y el oponente es tier2). 
- Si soy tb tier2, no va a afectar, ya que sería 1d20 +3 +3 vs DC 10 +4 +3. Peeero, si yo soy tier3 sería 1d20 +3 +4 vs DC 10 +4 +3 (he ganado un +1 xq soy más poderoso que el otro caster).
-  Y lo mismo para Disipar Magia. Si no tengo referencia del lanzador/creador de lo que se quiera disipar, basta con darle el tier necesario para lanzar ese conjuro o ver la DC que aplica, que suele dar la pista.


### Creación de pergaminos
- Pueden escribir pergaminos con sus conjuros para uso personal (no valen como pergamino estándar para cualquiera) al precio que les cuesta copiarlos en un libro de conjuro de apoyo. De hecho, podrían escribir directamente en un libro esas copias.
- Ya que un mago puede usar directamente un conjuro de su libro, lo que consumiría ese escrito y la página quedaría inutilizada, pero es un buen recurso a su disposición.



### Curaciones mágicas (poderes, conjuros y potis)
- Funcionan de base como los críticos:
    - Se tiran los dados, se suma lo que toque sumar, y se **multiplica por dos el total**


## Maniobras
- No hay bonus a las maniobras conocidas por clase.
- Los personajes conocen tres maniobras del Battlemaster del manual del jugador de 5ª, y pueden activarlas gastando un punto de acción.
- Como acción adicional, se pueden usar las maniobras que tienen las armas.


## Equipo
- Se añade el Stiletto/[Daga de Misericordia](https://en.wikipedia.org/wiki/Misericorde_(weapon))
    - Cuenta como daga
    - 1d4 de daño perforante
    - Características: Finesse, Light
    - Maniobras (acción adicional):
        - Armor Piercing: si el objetivo lleva armadura media o pesada, ganas +1d4 al daño y en caso de posible crítico, esa tirada se suma a la dificultad de su TS
        - Fatality: si un objetivo está derribado y puedes identificar los puntos débiles de su armadura y estás adyacente, baja el rango de crítico en 2
- Se pueden tener sintonizados un máximo de objetos mágicos igual a 3 + Competencia.
- Se considera que la espada larga tiene la etiquera **Versátil**
    - Espada larga: 1d8, Defensiva (medium), Versatile (1d10) - Slashing

### Armaduras
- Se pueden tener piezas de armadura estéticas, en el sentido de que no bonifican la CA, pero pueden dar una utilidad a la hora de resolver los críticos.
    - Si la localización del crítico es dónde hay una pieza de armadura o escudo, esta pieza se puede llevar el efecto del crítico.
- Escudos ligeros de manos libres: la mano libre no se puede usar para empuñar armas, excepto las que se describen como **hand-mounted** (El stiletto valdría)
    - También permite realizar el componente somático de los conjuros, o realizar otras acciones con esa mano.
- **Adamantita**:  respecto a la propiedad Hardy, me lo he replanteado al leerlo. 
    - Como va asociado a las mecánicas de mantenimiento de la armadura, vamos a mantenerlo como está (un uso desde la última vez que hiciste mantenimiento). 
    - Osea, la adamantita al ser Hardy, tiene esta capacidad. Que de +1d4 de pericia en la TS Armadura sigue siendo válido, además de que puedas librarte automáticamente del primer crítico desde el último mantenimiento.

## Combate
### Derribos y caídas
- Quien caiga tiene como efecto colateral tirar una TS CON para evitar un turno incapacitado.
    - Si tienes una maniobra posterior a la caída, tienes primero que pasar la TS CON para no perder tu reacción y emplearla.
    - Alguien entrenado en Acrobacias puede pedir utilizar su reacción y un punto de acción para hacer una acrobacia y aterrizar sin problema.
    - También se acepta gastar un punto de acción para activar una maniobra que sirva como reacción a la caída.

### Críticos y Pifias
- En Level Up el daño crítico es hacer la tirada de daño normal, y multiplicar el resultado por dos.
- Voy a emplear alguna tabla para cuando te comes un crítico, de efectos críticos. 
- Hay una TS de armadura para evitar el efecto (el daño no, ese como siempre ñam ñam a comer). 
- No depende de característica, sino de la armadura que llevas. 
- Si no tienes competencia, daría la mitad. De normal, tienes lo mismo que te está dando al CA tu armadura. 
- Si llevas escudo, también suma. 
- Rasgos y cosas que requieran que estés consciente para usarlo, no. 
- Objetos de efecto permanente, sí. Piedra de la Suerte, por ejemplo.
- Clases como bárbaro o monje: suman las dos características que les da la armadura si están en condiciones de usar esa defensa.
- Ejemplo:
    - Osea, me cae un crítico y tengo Des 16 (+3CA) cuero tachonado (+2CA) y escudo (+2CA) y yelmo (irrelevante al CA). 
    - Tiro TS Armadura  1d20 + 7 contra la DC Maniobra de mi atacante (digamos 8 + 2 + 3 = 13). 
    - Ya me llevo antes de tirar esto, el doble del daño de su ataque... y si fallo la TS puede que la hostia vaya a la cabeza y me venga bien el yelmo para evitar algo peor...
- Osea, la localización del golpe solamente importa cuando hay que leer el efecto crítico xq haya entrado
- La ADAMANTITA da bonus de pericia según el tipo de armadura: ligera +1d4, media +1d6, pesada +1d8
    - una adamantita encantada (como la de los drow) o una armadura mágica puede mejorar ese bonus
- No hay absorción de daño por armaduras.
- Ganar la espalda baja el rango crítico en 2
    - Requiere ganar la espalda, no simplemente colocar la mini a la espalda.
        - Esto se consigue mediante engaño, distracciones, situaciones de mala visibilidad, rasgos,...
- Límite inferior del rango crítico: 
    - 17 por aptitudes y maniobras
    - Se puede bajar un punto más hasta 15 en determinados contextos.

### Amenaza de crítico
- El valor de la amenaza de crítico baja en 1 por tier de la criatura: 1 si es tier 1, 2 si es tier 2,...
- Ejemplo:
    - Nivel 8, el crítico es 20, y la amenaza de crítico es 18-19
    - En caso de sacar 18-19 natural, el objetivo debe hacer una TS Armadura para evitar el efecto crítico, pero el daño no se ve afectado
- Si la amenaza de crítico (ej.: 18 en el dado, +4 al ataque = 22) no bastase para impactar a la criatura (CA23), no se produce la amenaza de crítico, aunque sí que se produce el impacto y el daño normal del arma.


### Mapa
- Las casillas del mapa son una simple referencia, no son celdas de una tabla. 
- Si hay una duda por 5 pies, me la suda la casilla, tiramos Suerte y lo que diga el dado. 
- Lo mismo lo digo por el movimiento de alguien, una bola de fuego o un cono... si está cerca de los límites abstractos de la supuesta área del efecto, tira Suerte. 
- El caster que quiera que sus oponentes fallen sin tirar y se resuelva como que entran en el efecto, que gasten 1 pto Suerte por cada oponente a jorobar.
- En resumen, las casillas no son un absoluto, son una referencia y se tira suerte en caso de duda.
- Y considerad que la gente no está parada, a menos que su movimiento ese asalto fuese 0. Generalmente, todos se mueven a la vez y la posición de la mini no puede considerarse estática.
- Y para según qué áreas de 20 pies de radio en adelante, voy a aplicar cierta variedad en el centro o en el borde del área.

### Flanqueo
-  Cuando son 3+ rodeándote, estás flanqueado.
-  Si son 2, y tu movimiento no es 0, tienes derecho a TS Des contra DC maniobra del mejor flanqueador de los 2 que están tratando de pillarte en medio.
- Lo que ocurre, es que si la cosa sigue igual al asalto siguente, pueden insistir en ello y te toca volver a salvar para evitarlo... sal de ahí!

### Evitar lanzamiento de conjuros
-  se puede preparar acción para atacar a un objetivo que previsiblemente fuer a alanzar un conjuro, para tratar de cortarle la concentración en pleno casteo... si le haces daño, tiene que hacer TS Con para que no le corte el rollo. 
- Si falla, se pierde ese conjuro y le va a tocar hacer otro chequeo de concentración por los conjuros que llevase activos (esta es la excepción a la regla de que fallar un chequeo de concentración te hace perder todos los conjuros en los que estuvieses concentrado. 
- En este caso, tratamos el conjuro que está siendo lanzado a parte del resto)


### Caos y sorpresa
- Por otra parte, en previsión de posibles situaciones en mesa, me adelanto en buscar una solución para algo que va a pasar. 
- Que alguien o algo actúe de repente y se dispare una situación que requiera tirar iniciativas. 
- Primero, ya conocemos todos cómo va lo del asalto de sorpresa, hasta ahí ok, resuelve muchos casos aplicar simplemente raw. 
- El problema lo veo cuando A de la nada dice que lanza un conjuro sobre B. No tiene mucho sentido que las 3 criaturas que van antes de A (no se sorprenden y sacan mejor iniciativa) puedan moverse decenas de pies, sacar las armas, hacer 4 ataques y lanzar un conjuro de ac.adicional (soltando un discurso sobre blablabla de paso). 
- Esto antes de que llegue a pasar la acción desencadenante de la situación, que era que A lance su conjuro. 
- Así que voy a ponerle un límite claro a lo que se puede hacer en anticipación a algo que se supone que YA está ocurriendo. 
- Los que actúan antes que la acción desencadenante, pueden anticiparse a ella como si hubiesen declarado Preparar Acción y pueden usar su reacción para usar esa acción preparada. 
- Tomo el texto del Acelerar, que va al pelo, esto es lo que se puede hacer: Esta acción puede utilizarse para realizar un único ataque con arma, o para llevar a cabo la acción de Correr, Destrabarse, Esconderse o Utilizar un objeto. 
    - Y añado, o Lanzar Conjuro.
- Osea, al final, al restringir a una especie de Acción Preparada, lo que ocurre es que los que se anticipan a la acción de A actúan como si estuviesen sobre aviso y pudieran prepararse para lo que iba a hacer A en un lapso de tiempo fugaz.
- La de perspicacia me parece muy muy bien para sustituir a Percepción en situaciones donde lo que importe sea leer el comportamiento de una criatura. Ejemplos:
    - un tío oculto/invisible ataca de repente - Percepción
    - Un tío con el que estabas hablando saca un cuchillo y trata de clavárselo a tu compi - Perspicacia.
    - Si era que estaba ocultando el cuchillo en la manga, permitiría elegir entre Percepción y Perspicacia


## Monstruos y criaturas
### No-muertos
- Leyendo el compendio de Lup, he caído en que va siendo hora de arreglar una injusticia.
-  Los undead (salvo excepciones, que tienen órganos sensoriales funcionales) no usan los sentidos de un ser vivo.
- Son conscientes de su entorno y de otras criaturas de forma sobrenatural (hay quien habla de habilidades psiónicas, pero los detalles no importan ahora...). 
- El caso es que lo voy a tratar como si fuera Visión Ciega, lo que les devuelve parte de su antigua gloria. 
- Pero aún así tienen mente (algunos escasa) y pueden ser afectados por efectos que manipulen mentes, daño psíquico y miedo. 
- Daño psíquico y debufos varios funcionan con normalidad, pero para otros efectos mágicos como Hechizar, Amistad, Ordenar o Invisibilidad ante muertos vivientes, requiere usar variantes de conjuro específicas para actuar sobre ellos. 
- La mayoría de ellas son fáciles de conseguir.
- Mente en Blanco haría que los undead no te distingan de un mueble, pero es un capricho muy caro




## Puntos de acción
- La reserva inicial es de 1d3 + nivel.
    - Al subir de nivel, se resetean, volviendo a tirar 1d3 + nivel.
- Se pueden emplear en lugar de puntos de esfuerzo (Exertion) para activar maniobras.
- Con un punto de acción se puede activar una de las tres maniobras de Battlemaster conocidas. En este caso funcionan como un dado de superioridad de d6.
- Con un punto de acción, se puede potencias en uno el nivel de un conjuro lanzado, o el de un truco en un tier.
    - Si se quiere potenciar un conjuro por encima de lo que normalmente se puede (por ejemplo a nivel 8 lanzar un conjuro como si fuese de nivel 5), habrá que tirar por la característica mágica del lanzador: DC 10 + nivel al que se quiere lanzar el conjuro.
        - Si se falla la tirada pueden haber complicaciones.
- Según el contexto, se permite estirar las reglas haciendo algo fuera del reglamento gastando un punto de acción.

### Recuperación de los Puntos de Acción (p.a.)
- Por sucesos en la partida, logros, escenas dramáticas y descubrimientos y avances, se pueden obtener p.a, pero fuera de este sistema de recompensa circunstancial, la mecánica básica para la recuperación de los p.a gastados se basa en acumular una reserva de p.a. Esta reserva se nutre de los p.a que el dm añade a un cuenco (representados como tokens) y revisa en cada encuentro de combate. Al inicio del encuentro, si hay suficientes tokens para generar p.a (n.º tokens / n.º jugadores) se reparten los p.a generados. La reserva continúa creciendo ese encuentro y los posteriores y al inicio combate, se revisa la generación de p.a. Un encuento que no sea de combate no genera p.a pero sí puede sumar tokens a la reserva. 
- Un descanso corto resta tokens a la reserva. Tantos como pjs / pnjs / seguidores haya en el grupo. Esto genera 1 p.a para esas criaturas.
- Un descanso largo no resta tokens. Al final de la jornada se cobra todo el acumulado, generando p.a con lo que haya en la reserva. El resto de la división entera se reparte entre los pnjs. 

### Uso de los Puntos de Acción
- Se pueden gestionar para plantear realizar una acción que se salga en algún aspecto de lo contemplado por las reglas. Generalmente, se trataría de algo que conlleva realizar un chequeo de habilidad o TS para conseguir su propósito, además de invertir el p.a.
– Se pueden invertir en potenciar el lanzamiento de un conjuro o habilidad innata. Por 1 p.a se considera que se lanza a un nivel superior. Cada p.a equivale a +1 nivel. Si se supera al nivel máximo de lanzamiento, se debe realizar un chequeo de habilidad conjuradora (CD 10 + nivel conjuro). El fallo de esta tirada supone arriesgarse a tirar en la Tabla de Pifias Mágicas. 
– Se pueden invertir en potenciar el lanzamiento de un cantrip. Se considerará lanzado a un tier superior, el siguiente escalón en la mejora del cantrip. El límite es de 1 p.a.
– Se pueden invertir a modo de Dado de Superioridad (1d6) para realizar una de las 3 maniobras de Battlemaster que conoce el pj.
- Se pueden invertir en lugar de puntos de esfuerzo (exertion). En cualquier uso de esos 


## Puntos de suerte
- Si te digo que tires Suerte, es 1d20 más tu bonus (ejm, con 14 sería +2). 
- Hay un pool de ptos suerte que se puede usar de varias formas.
    - Para tener ventaja en tiradas de Suerte. 
    - para que un pnj falle directo Suerte si era para caer dentro del área de tu efecto. 
    - Para que se repita una tirada, de lo que sea, de quien sea... mientras sea la 1ºvez en esa sesión que se invoca para ese tipo de tirada
- No hay una cifra estándar, es un premio ad hoc.
    - Pero estoy considerando daros vuestro bono cada mañana (+1 adicional humanos y medianos). 
    - Así que los nuevos podéis comenzar con el doble de eso... osea, soy humano con Suerte 14 (+2) voy a salir con 6 ptos


## Venenos
- [Descripción de los venenos](./resources/venenos.md)
- [Fabricación de venenos](./resources/fabricación-venenos.md)



## Otros
### Tritaja
- Tenemos tritaja, lo que sería ventaja + ventaja ... o sea, tirar 3d20 ... pero la máxima de 5ed de que da igual cuántas fuentes hay de ventaja o desv. 
- Si hay colisión, simplemente se anulan y se tira solamente 1d20

### Pociones
- Las pociones duran 1d4+1 horas

puntos.



### Pasivas
- Las habilidades pasivas, como percepción pasiva, tienen un valor de 8 + COMPETENCIA + MOD CARACTERÍSTICA

### Características
- En la creación el límite es de 19 con los bonos de creación
- En nivel 4 el tope es 20
- Con las dotes que dan rasgos se puede subir hasta 22.
    - No se puede subir de 20 hasta 22 mediante ASIs, únicamente mediante dotes.
- Las dotes que pongan que sube hasta un máximo de 20, considerad que pone 22.
- Mediante objetos mágicos y conjuros también se puede superar el límite de 22.


### Descansos cortos
- Borrar una ración de comida/agua en cada descanso corto.
- Usando **incienso de meditación** y gastando los bocatas (dados de golpe) en los descansos cortos, se pueden recuperar espacios de conjuros.
    - Sirve tanto para progresión de conjuros como para los innatos o de dotes.
    - Se sacrifica dados de golpe por casillas de conjuro, a razón de 1DG por nivel.


### Downtime / Tiempo entre aventuras
- Me voy a basar tanto en Lup como en Xanathar, que es bastante compatible. 
- El tiempo de referencia es el de Forgotten, la decana (10 días). 
- Dependiendo de lo que queráis hacer, puedo acortarlo o alargarlo. 
- Hay cosas que tal vez podáis meter 2 en la misma decana y hay otras que tal vez requieran unos cuántos pasos, chequeos y decisiones y lleven varias decanas (las prisas pueden servir a veces, a cambio de más riesgo o dificultad).  

### Tiradas de salvación de objetos
![Tirada de salvación de los objetos](./resources/TS-Objetos.jpg)
